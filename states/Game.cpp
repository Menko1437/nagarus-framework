#include "../states/Game.hpp"
#include "../include/Core.hpp"

// components
#include "../components/Position.hpp"
#include "../components/Velocity.hpp"
#include "../components/Renderable.hpp"

#include "../systems/MovementControl.hpp"
#include "../systems/RenderSystem.hpp"

#include "../math/Vectors.hpp"

#include <thread>

#include "../include/spdlog/spdlog.h"

Game Game::m_GameState;

void Game::Init(Core *game){
    spdlog::info("Initializing game..");

    // entityx
    game->ECS_Systems().add<MovementControl>();
    game->ECS_Systems().add<RenderSystem>();
    game->ECS_Systems().configure();


    m_Event = std::make_unique<sf::Event>();
    m_Clock = std::make_unique<sf::Clock>();
    gui = std::make_unique<tgui::Gui>();
    gui->setTarget(game->getWindow());

    m_TextureManager = std::make_unique<TextureManager>();
    m_TextureManager->SetResourceDir("../img/");
    m_AudioManager = std::make_unique<AudioManager>();
    
    e_Player = game->ECS_Entities().create();
    e_Player.assign<PositionComponent>(400.0f,100.0f);
    e_Player.assign<VelocityComponent>(0.0f,0.0f);
    e_Player.assign<Renderable>(game->getWindow(),*m_TextureManager->GetTexture("goblinIdle.png"));

}

void Game::Cleanup(){
    spdlog::info("Cleaning game..");
}

void Game::Pause(){
    spdlog::info("Game paused.");
}

void Game::Resume(){
    spdlog::info("Game resumed.");
}

void Game::HandleEvents(Core *game){
    while(game->getWindow().pollEvent(*m_Event)){
        if (m_Event->type == sf::Event::Closed){
            game->Quit();
        }

        if(m_Event->type == sf::Event::KeyPressed){
            if(m_Event->key.code == sf::Keyboard::Escape){
                game->Quit();
            }
        }
            
        if(m_Event->type == sf::Event::Resized){
            gui->setView(game->getWindow().getView());
        }


    gui->handleEvent(*m_Event);
    }
}

void Game::Update(Core *game){
    deltaTime = m_Clock->restart().asSeconds();

    game->ECS_Systems().update<MovementControl>(deltaTime);
    
}

void Game::Draw(Core *game){
    game->getWindow().clear(sf::Color::White);

    game->ECS_Systems().update<RenderSystem>(deltaTime);

    gui->draw(); 
    game->getWindow().display();
}