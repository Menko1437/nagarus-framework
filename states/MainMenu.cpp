#include "../states/MainMenu.hpp"
#include "../states/Game.hpp"
#include "../states/Settings.hpp"

#include "../include/spdlog/spdlog.h"

MainMenu MainMenu::m_MainMenuState;

void MainMenu::Init(Core *game){
    spdlog::info("Initializing Main Menu");

    m_Event = std::make_unique<sf::Event>();

    gui.setTarget(game->getWindow());
    
    btnStart->setSize(100,30);
    btnStart->setText("Start");
    btnStart->setPosition((game->getWindowSize().x / 2) - 50, game->getWindowSize().y / 2);
    gui.add(btnStart);

    btnSettings->setSize(100,30);
    btnSettings->setText("Settings");
    btnSettings->setPosition((game->getWindowSize().x / 2) - 50, game->getWindowSize().y / 2 + 50);
    gui.add(btnSettings);

    btnExit->setSize(100,30);
    btnExit->setText("Exit");
    btnExit->setPosition((game->getWindowSize().x / 2) - 50, game->getWindowSize().y / 2 + 100);
    gui.add(btnExit);
    
    btnStart->connect("pressed", [=](){ game->ChangeState( Game::Instance() ); });
    btnSettings->connect("pressed", [=](){ game->PushState( Settings::Instance() ); });
    btnExit->connect("pressed", [=](){ game->Quit(); });
} 
void MainMenu::Cleanup(){
    spdlog::info("Cleaning Main Menu..");
}

void MainMenu::Pause(){

}
void MainMenu::Resume(){

}

void MainMenu::HandleEvents(Core *game){
    while(game->getWindow().pollEvent(*m_Event)){
        if (m_Event->type == sf::Event::Closed){
            game->Quit();
        }
        if(m_Event->type == sf::Event::Resized){
            gui.setView(game->getWindow().getView());
        }

        gui.handleEvent(*m_Event);
    }
}

void MainMenu::Update(Core *game){

}
void MainMenu::Draw(Core *game){
    game->getWindow().clear(sf::Color::White);
    
    
    gui.draw();
    game->getWindow().display();
} 