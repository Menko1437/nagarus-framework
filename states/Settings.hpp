#ifndef SETTINGS_HPP
#define SETTINGS_HPP

// std
#include <iostream>
#include <vector>

// 3rd party
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// local
#include "../include/GameState.hpp"


class Settings : public GameState{
public:
	void Init(Core *game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(Core *game);
	void Update(Core *game);
	void Draw(Core *game);

	static Settings* Instance() {
		return &m_SettingsState;
	}

protected:
	Settings() { }

private:
	static Settings m_SettingsState;

    std::unique_ptr<sf::Event> m_Event;

	//gui
	std::unique_ptr<tgui::Gui> gui;

	// Anti aliasing
	tgui::Label::Ptr m_lblAntiAliasing = tgui::Label::create();
    tgui::Label::Ptr m_lblAntiAliasingCurrent = tgui::Label::create();
    tgui::Slider::Ptr m_sAntiAliasing = tgui::Slider::create();
	uint32_t m_CurrentAA = 0;
	bool m_AAChanged = false;

    // Frame Per Second
    tgui::Label::Ptr m_lblFps = tgui::Label::create();
    tgui::Label::Ptr m_lblFpsCurrent = tgui::Label::create();
	tgui::Label::Ptr m_lblFpsShow = tgui::Label::create();
    tgui::Slider::Ptr m_sFps = tgui::Slider::create();
    uint32_t m_CurrentFPS = 60;
    bool m_FpsChanged = false;
    
    // Virtual Sync
    tgui::CheckBox::Ptr m_cbVsync = tgui::CheckBox::create();
    tgui::Label::Ptr m_lblVsync = tgui::Label::create();

	// Back button
	tgui::Button::Ptr m_backButton = tgui::Button::create();

	std::unique_ptr<sf::Clock> clock;
     
};


#endif // !SETTINGS_HPP