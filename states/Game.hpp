#ifndef GAME_HPP
#define GAME_HPP

// std
#include <iostream>

// 3rd party
#include <entityx/entityx.h>
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// local
#include "../include/GameState.hpp"
#include "../include/TextureManager.hpp"
#include "../include/AudioManager.hpp"
#include "../include/Sound.hpp"
#include "../include/Animation.hpp"


class Game : public GameState{
public:
	void Init(Core *game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(Core *game);
	void Update(Core *game);
	void Draw(Core *game);

	static Game* Instance() {
		return &m_GameState;
	}

protected:
	explicit Game() { }

private:
	static Game m_GameState;
	float deltaTime = 0.0f;

	// sfml
    std::unique_ptr<sf::Event> m_Event;
	std::unique_ptr<sf::Clock> m_Clock;

	// Resource managers
    std::unique_ptr<TextureManager> m_TextureManager;
    std::unique_ptr<AudioManager> m_AudioManager;
	
	// entities
	entityx::Entity e_Player;

	//gui
	std::unique_ptr<tgui::Gui> gui;
};  
 
#endif // !GAME_HPP