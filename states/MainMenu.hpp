#ifndef MAINMENU_HPP
#define MAINMENU_HPP

// std
#include <iostream>

// 3rd party
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// local
#include "../include/GameState.hpp"

class MainMenu : public GameState{
public:
	void Init(Core *game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(Core *game);
	void Update(Core *game);
	void Draw(Core *game);

	static MainMenu* Instance() {
		return &m_MainMenuState;
	}

protected:
	MainMenu() { }

private:
	static MainMenu m_MainMenuState;
    std::unique_ptr<sf::Event> m_Event;

	//gui
	tgui::Gui gui;
    tgui::Button::Ptr btnStart = tgui::Button::create();
    tgui::Button::Ptr btnSettings = tgui::Button::create();
    tgui::Button::Ptr btnExit = tgui::Button::create();
};


#endif // !MAINMENU_HPP