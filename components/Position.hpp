#ifndef POSITION_HPP
#define POSITION_HPP

#include <entityx/entityx.h>

#include "../math/Vectors.hpp"

struct PositionComponent : entityx::Component<PositionComponent>{
    PositionComponent(float x = 0.0f, float y = 0.0f) :  position{x,y} {} 

    Vec2Df position;
};



#endif // !POSITION_HPP