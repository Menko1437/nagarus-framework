#ifndef RENDERABLE_HPP
#define RENDERABLE_HPP

#include <entityx/entityx.h>

#include <SFML/Graphics.hpp>

struct Renderable : entityx::Component<Renderable>{
  Renderable(sf::RenderWindow &window, sf::Texture &tex) : target(window),texture(tex){ }
  sf::RenderWindow &target;
  sf::Texture &texture;
  sf::Sprite sprite;
};


#endif // !RENDERABLE_HPP