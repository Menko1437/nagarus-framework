#ifndef RENDERSYSTEM_HPP
#define RENDERSYSTEM_HPP

#include <memory>
#include <entityx/entityx.h>


class RenderSystem : public entityx::System<RenderSystem>{
public:
    void update(entityx::EntityManager &es, entityx::EventManager &events, entityx::TimeDelta dt) override;
private:

};



#endif // !RENDERSYSTEM_HPP