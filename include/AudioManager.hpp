#ifndef AUDIOMANAGER_HPP
#define AUDIOMANAGER_HPP

// std
#include <iostream>
#include <vector>
#include <map>
#include <memory>

// 3rd party

// local
#include "../include/Sound.hpp"

class AudioManager{
public:
    AudioManager();
    ~AudioManager();

    void PlaySound(const std::string &soundName);
    void AddSound(const std::string &soundName,Sound *snd);

private:
    std::map<std::string , Sound *> m_Sounds;
};

#endif // !AUDIOMANAGER_HPP