#ifndef CORE_HPP
#define CORE_HPP

// std
#include <iostream>
#include <vector>
#include <memory>

// 3rd party
#include <SFML/Graphics.hpp>
#include <entityx/entityx.h>
#include <entityx/Event.h>
#include <entityx/Entity.h>
#include <entityx/System.h>
//local

typedef struct{
    int x,y;
}WindowSize;

class GameState;

class Core : public entityx::EntityX{
public:
    Core(std::string name, uint32_t width, uint32_t height, bool fullScreen);
    
    void Init();
    
    void ChangeState(GameState* state);
    void PushState(GameState* state);
    void PopState();

    void HandleEvents();
    void Update();
    void Draw();

    void Pause();
    void Resume();

    bool Running() { return m_Running; };
    void Quit() { m_Running = false; };

    entityx::SystemManager& ECS_Systems(){
        return systems;
    }

    entityx::EntityManager& ECS_Entities(){
        return entities;
    }

    entityx::EventManager& ECS_Events(){
        return events;
    }

    sf::RenderWindow& getWindow(){
        return *m_Renderer;
    }

    WindowSize& getWindowSize(){
        return winSize;
    }

    sf::ContextSettings& getSettings(){
        return *m_Settings;
    }

    void Cleanup();
private:

    WindowSize winSize;

    bool m_Running;
    bool m_FullScreen;

    std::vector<GameState*> states;

    std::unique_ptr<sf::ContextSettings> m_Settings;
    std::unique_ptr<sf::RenderWindow> m_Renderer;
};

#endif // CORE_HPP