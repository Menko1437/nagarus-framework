#include "../include/AudioManager.hpp"

#include "../include/spdlog/spdlog.h"

AudioManager::AudioManager(){ };
AudioManager::~AudioManager(){
    for(auto &e : m_Sounds){
        delete e.second;
    }
    m_Sounds.clear();  
}

void AudioManager::AddSound(const std::string &soundName , Sound *snd){
    m_Sounds.insert(std::pair<std::string,Sound *>(soundName,snd));
    spdlog::info("Added sound: {0}" , soundName);
}

void AudioManager::PlaySound(const std::string &soundName){
    std::map<std::string, Sound *>::iterator it;
    it = m_Sounds.find(soundName);
    if(it != m_Sounds.end()){
        it->second->sound->play();
    }
}