#include "../include/Sound.hpp"

Sound::Sound(){ 
    buffer = std::make_unique<sf::SoundBuffer>();
    sound = std::make_unique<sf::Sound>();
};
Sound::~Sound() {

};


bool Sound::loadFromFile(const std::string &filename){
    if(!buffer->loadFromFile(filename)){
        return false;
    }

    sound->setBuffer(*buffer);

    return true;
}