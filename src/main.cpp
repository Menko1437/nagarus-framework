// std
#include <iostream>
#include <vector>
#include <memory>

// 3rd party
#include <SFML/Graphics.hpp>
#include "../include/spdlog/spdlog.h"

// local
#include "../include/Core.hpp"
#include "../states/Intro.hpp"


int main(){
   
    
    spdlog::info("Starting nagarus framework...");
    std::unique_ptr<Core> window = std::make_unique<Core>("Framework",800,640,false);
    
    spdlog::info("Initializing Core...");
    window->Init();

    spdlog::info("Changing state to Intro");
    window->ChangeState(Intro::Instance());

    while(window->Running()){
        window->HandleEvents();
        window->Update();
        window->Draw();
    }


    spdlog::info("Cleaning everything up...");
    window->Cleanup();


    return 0;
}