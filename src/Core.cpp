#include "../include/Core.hpp"
#include "../include/GameState.hpp"

#include "../include/spdlog/spdlog.h"

Core::Core(std::string name, uint32_t width, uint32_t height, bool fullScreen) : m_Running(true), m_FullScreen(fullScreen){
   
    m_Renderer = std::make_unique<sf::RenderWindow>(sf::VideoMode(width,height),name);

    winSize.x = width;
    winSize.y = height;

}

void Core::Init(){
    
    // should be fixed to load from settings file ( TODO )
    m_Renderer->setFramerateLimit(60);
    m_Renderer->setActive(true);
    m_Renderer->setVerticalSyncEnabled(false);

    m_Settings = std::make_unique<sf::ContextSettings>();

    if(m_FullScreen){
        // .. set full screen mode ( TODO )
    }

}

void Core::HandleEvents(){
    states.back()->HandleEvents(this);
}



void Core::Update(){
    states.back()->Update(this);
}


void Core::Draw(){
    states.back()->Draw(this);
}

// remove all states and push new
void Core::ChangeState(GameState* state) 
{
	if (!states.empty()) {
		states.back()->Cleanup();
		states.pop_back();
	}


	states.push_back(state);
	states.back()->Init(this);
}

// push new state on top of states
void Core::PushState(GameState* state)
{

	if ( !states.empty() ) {
		states.back()->Pause();
	}

	states.push_back(state);
	states.back()->Init(this);
}

void Core::PopState()
{
	// cleanup the current state
	if ( !states.empty() ) {
		states.back()->Cleanup();
		states.pop_back();
	}

	if ( !states.empty() ) {
		states.back()->Resume();
	}
}

void Core::Cleanup(){
    while(!states.empty()){
        states.back()->Cleanup();
        states.pop_back();
    }

    spdlog::info("Releasing core..");

    m_Running = false;

    if(m_Renderer->isOpen()){
        m_Renderer->close();
    }
}

void Core::Pause(){
    spdlog::info("Paused..");
}

void Core::Resume(){
    spdlog::info("Resumed..");
}