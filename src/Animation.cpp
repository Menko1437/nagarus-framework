#include "../include/Animation.hpp"

Animation::Animation(const sf::Texture *texture,sf::Vector2u imageCount,float switchTime){ 
    this->m_ImageCount = imageCount;
    this->m_SwitchTime = switchTime;
    m_TotalTime = 0.0f;

    m_CurrentImage.x = 0;

    m_SpriteRect.width = texture->getSize().x / static_cast<float>(m_ImageCount.x);
    m_SpriteRect.height = texture->getSize().y / static_cast<float>(m_ImageCount.y);
}
Animation::~Animation(){

}

void Animation::Update(int row,float dt, bool faceLeft){
    m_CurrentImage.y = row;
    m_TotalTime += dt;

    if(m_TotalTime >= m_SwitchTime){
        m_TotalTime -= m_SwitchTime;
        m_CurrentImage.x++;
        if(m_CurrentImage.x >= m_ImageCount.x){
            m_CurrentImage.x = 0;
        }
    }
    
    m_SpriteRect.top = m_CurrentImage.y * m_SpriteRect.height;

    if(faceLeft){
        m_SpriteRect.left = m_CurrentImage.x * m_SpriteRect.width;
        m_SpriteRect.width = std::abs(m_SpriteRect.width);
    }else{
        m_SpriteRect.left = (m_CurrentImage.x + 1) * std::abs(m_SpriteRect.width);
        m_SpriteRect.width = -std::abs(m_SpriteRect.width);
    }
}

