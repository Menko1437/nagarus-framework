#ifndef VECTORS_HPP
#define VECTORS_HPP

#include <memory>

struct Vec2Di{
    Vec2Di(){}
    Vec2Di(int32_t a, int32_t b){x = a, y = b;}
    int32_t x,y;
    inline Vec2Di operator=(int a){
        Vec2Di b;
        b.x = a;
        b.y = a;
    return b;
    }
};

struct Vec2Df{
    Vec2Df(){}
    Vec2Df(float a, float b){x = a, y = b;}
    float x,y;
    inline Vec2Df operator=(int a){
        Vec2Df b;
        b.x = a;
        b.y = a;
    return b;
    }
};

struct Vec2Du{
    Vec2Du(){}
    Vec2Du(uint32_t a, uint32_t b){x = a, y = b;}
    uint32_t x,y;
    inline Vec2Du operator=(int a){
        Vec2Du b;
        b.x = a;
        b.y = a;
    return b;
    }
};



#endif // !VECTORS_HPP